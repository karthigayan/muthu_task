import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

  {
    path: '/event',
    title: 'Event',
    icon: 'fa fa-calendar',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
       ]
  },
  {
    path: '/map',
    title: 'Map',
    icon: 'fas fa-map',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
      ]
  }
];
