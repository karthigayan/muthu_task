import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderNavigationComponent } from './header-navigation/header-navigation.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import {NgbAccordionModule, NgbCarouselModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SpinnerComponent} from './spinner.component';

@NgModule({
  declarations: [SidebarComponent, HeaderNavigationComponent, BreadcrumbComponent, SpinnerComponent],
    imports: [
        CommonModule,
        NgbDropdownModule,
        RouterModule,
        PerfectScrollbarModule,
        NgbCarouselModule,
        NgbAccordionModule
    ],
    exports: [SidebarComponent, HeaderNavigationComponent, BreadcrumbComponent, SpinnerComponent]
})
export class LayoutsModule { }
