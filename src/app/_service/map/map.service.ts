import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }
/*  getOverviewCount(): Observable<any> {
    return this.http.get('http://3.212.77.232:8080/api/event/list?page=0&size=10').pipe(
        map(data => {
          return data;
        })
    );
  }*/
    uploadFile(formData) {
        return this.http.post('http://3.212.77.232:8080/api/ip/list', formData, {
            reportProgress: true,
            observe: 'events'
        });
    }

    postFile(fileToUpload: File): Observable<any> {
        const endpoint = 'http://3.212.77.232:8080/api/ip/list';
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.http.post(endpoint, formData).pipe(
            map(data => {
            return data;
        })
     );
    }
}
