import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  getOverviewCount(page, size, search): Observable<any> {
    return this.http.get('http://3.212.77.232:8080/api/event/list?page=' + page + '&size=' + size + '' + '&search=' + search,
        {
          headers: {'Access-Control-Allow-Origin': '*' }
        }
        ).pipe(
      map(data => {
        return data;
      })
    );
  }

}
