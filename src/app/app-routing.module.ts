import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FullComponent} from './_shared/components/pages/full/full.component';
import {BlankComponent} from './_shared/components/pages/blank/blank.component';

export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {path: '', redirectTo: 'event', pathMatch: 'full'},
      {path: 'event', loadChildren: './_components/event/event.module#EventModule'},
      {path: 'map', loadChildren: './_components/map/map.module#MapModule'},
      {path: 'attribute', loadChildren: './_components/event-attribute/event-attribute.module#EventAttributeModule'},
      {path: 'Login', loadChildren: './_components/login/login.module#LoginModule'}
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [

    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AppRoutingModule { }
