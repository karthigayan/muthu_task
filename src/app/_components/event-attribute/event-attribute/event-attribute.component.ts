import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {EventService} from '../../../_service/event.service';

@Component({
  selector: 'app-event-attribute',
  templateUrl: './event-attribute.component.html',
  styleUrls: ['./event-attribute.component.css']
})
export class EventAttributeComponent implements OnInit {

  result: any;

  constructor(private router: Router) { }

  ngOnInit() {
    this.result = JSON.parse(localStorage.getItem('attribute'));
  }

  backClick() {
    this.router.navigate(['event']);
  }

}
