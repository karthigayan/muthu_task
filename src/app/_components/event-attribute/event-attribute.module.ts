import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventAttributeRoutingModule } from './event-attribute-routing.module';
import { EventAttributeComponent } from './event-attribute/event-attribute.component';
import {MaterialModuleModule} from '../../_shared/components/material-module/material-module.module';


@NgModule({
  declarations: [EventAttributeComponent],
    imports: [
        CommonModule,
        EventAttributeRoutingModule,
        MaterialModuleModule
    ]
})
export class EventAttributeModule { }
