import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventAttributeComponent} from './event-attribute/event-attribute.component';


const routes: Routes = [
  {path: '', component: EventAttributeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventAttributeRoutingModule { }
