import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map/map.component';
import {AgmCoreModule} from '@agm/core';


@NgModule({
  declarations: [MapComponent],
    imports: [
        CommonModule,
        MapRoutingModule,
        AgmCoreModule
    ]
})
export class MapModule { }
