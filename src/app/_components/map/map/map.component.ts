import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import {MapService} from '../../../_service/map/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {
    public isSpinnerVisible = false;
    tempFile: any;
    fileToUpload: File = null;
    mapData: any;

    constructor(private mapService: MapService) {}
    // google maps zoom level
    zoom = 2;

    // initial center position for the map
    lat = 37.7749295;
    lng = -122.4194155;
    scroll = false;

/*    markers: marker[] = [
        { lat: 27.9994, lng: 120.6668, label: 'Zhejiang', draggable: true, visible: false, opacity: 0.7 },
        { lat: 22.7179, lng: 75.8333, label: 'Madhya Pradesh', draggable: true, visible: false, opacity: 0.7 },
        { lat: 22.7179, lng: 75.8333, label: 'Madhya Pradesh', draggable: true, visible: false, opacity: 0.7 },
        { lat: 31.2222, lng: 121.4581, label: 'Shanghai', draggable: true, visible: false, opacity: 0.7 }
    ];*/


/*    clickedMarker(label: string, index: number) {
        console.log('clicked the marker: ${label || index}');
        this.markers[index].visible = false;
    }

    mapClicked($event: MouseEvent) {
        this.markers.push({
            lat: $event.coords.lat,
            lng: $event.coords.lng,
            draggable: true,
            visible: true,
            opacity: 0.4
        });
    }*/

/*    markerDragEnd(m: marker, $event: MouseEvent) {
        console.log('dragEnd', m, $event);
    }*/

/*    onFileChange(event) {
        console.log(event.target.files[0]);
        this.tempFile = event.target.files[0];
        // this.fileSubmit(tempFile);
    }
    fileSubmit() {
        const formData = new FormData();
        formData.append('file', this.tempFile);
        this.mapService.uploadFile(formData).subscribe(res => {
            console.log(res.type);
        });
    }*/

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        this.uploadFileToActivity();
    }

    uploadFileToActivity() {
        this.isSpinnerVisible = true;
        this.mapService.postFile(this.fileToUpload).subscribe(data => {
            this.mapData = data.map(v => ({lat: v.lat, lng: v.lng, label: v.region, draggable: true, visible: false, opacity: 0.7  }));
            this.lat = this.mapData[0].lat;
            this.lng = this.mapData[0].lng;
            this.isSpinnerVisible = false;
            }, error => {
            this.isSpinnerVisible = false;
            console.log(error);
        });
    }
}
// just an interface for type safety.
// tslint:disable-next-line:class-name
interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
    visible: boolean;
    opacity: number;
}
