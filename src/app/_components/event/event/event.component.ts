import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventService} from '../../../_service/event.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {
  result: any;
  attribute: any;
  pageLength: any;
  pageSize: any;
  subscription: Subscription;
  p: number = 1;
  page: number;
  size: number;
  eventSearch = '';

  constructor(private event: EventService,
              private router: Router) { }

  ngOnInit() {
    this.page = 1;
    this.size = 10;
    this.subscription = this.event.getOverviewCount(this.page, this.size, this.eventSearch).subscribe(data => {
      /*this.result = data;*/
      // console.log(data);
      this.result = data.content;
      this.pageLength = data.totalElements;
      this.pageSize = data.size;
      console.log(this.result);
    });
  }

  onPaginateChange(event) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    this.result = [];
    this.subscription = this.event.getOverviewCount(event.pageIndex, event.pageSize, this.eventSearch).subscribe(data => {
      /*this.result = data;*/
      // console.log(data);
      this.result = data.content;
      this.pageLength = data.totalElements;
      this.pageSize = data.size;
      console.log(this.result);
    });
  }

  searchClick() {
    this.result = [];
    this.subscription = this.event.getOverviewCount(this.page, this.size, this.eventSearch).subscribe(data => {
      /*this.result = data;*/
      // console.log(data);
      this.result = data.content;
      this.pageLength = data.totalElements;
      this.pageSize = data.size;
      console.log(this.result);
    });
  }

  clickContent(value: any) {
    localStorage.setItem('attribute', JSON.stringify(value));
    this.attribute = value;
    console.log(value);
    this.router.navigate(['attribute']);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
