import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { EventComponent } from './event/event.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatPaginatorModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {MaterialModuleModule} from '../../_shared/components/material-module/material-module.module';


@NgModule({
  declarations: [EventComponent],
    imports: [
        CommonModule,
        EventRoutingModule,
        NgxPaginationModule,
        MatPaginatorModule,
        FormsModule,
        MaterialModuleModule
    ]
})
export class EventModule { }
